const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const books = require('./book/bookSqlApi');
const author = require('./author/authorSqlApi');
const { logger } = require('./middleware');

const PORT = process.env.PORT || '8000';

const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

app.use(logger);
app.use(cors());
app.get('/', (req, res) => {
  res.render('login', { message: '' });
});

app.use('/books', books);
app.use('/author', author);

// app.get('*', (req, res, next) => {
//   const err = new Error('Page Not Found');
//   err.statusCode = 404;
//   next(err);
// });
// // eslint-disable-next-line no-unused-vars
// app.use((err, req, res, _next) => {
//   res.render('./error', { error: err.message });
// });
// eslint-disable-next-line no-unused-vars
const server = app.listen(PORT, () => console.log('server started'));

module.exports = { app, server };
