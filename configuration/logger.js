const { createLogger, format, transports } = require('winston');

module.exports = createLogger({
  transports: [
    new transports.Console({
      level: 'info',
      format: format.combine(
        format.colorize(),
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.printf(info => `${info.message} ==> Time: [${info.timestamp}] , Loggerlevel:[${info.level}]`),
      ),
      transports: [new transports.Console()],
    }),
  ],
});
