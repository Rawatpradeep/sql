const express = require('express');
const logger = require('../configuration/logger');
const controller = require('./authorController');

const authorRouter = express.Router();

authorRouter.get('/api/getAuthors', async (_req, res) => {
  logger.info('Author Page entered');
  try {
    const author = await controller.getAuthors('author');
    if (author.length === 0) {
      return res.status(404).render('./error', { error: 'No Data Found' });
    }
    return res.json(author);
  } catch (error) {
    return res.status(404).render('error', { error });
  }
});
authorRouter.get('/api/getAuthor/:ID', async (req, res) => {
  try {
    const authors = await controller.getAboutAuthor(req.params.ID);
    if (authors.length === 0) {
      return res.status(404).render('./error', { error: 'InvalidBookID' });
    }
    return res.json(authors);
  } catch (errors) {
    return res.status(404).render('error', { error: errors });
  }
});

authorRouter.post('/add', async (req, res) => {
  try {
    const {
      // eslint-disable-next-line camelcase
      Author_NAME, ShortInfo,
    } = req.body;
    const result = await controller.addAuthor(Author_NAME, ShortInfo);
    if (result > 0) {
      // res.send('added successfully');
      res.json({ success: 'Data added successfully' });
    } else {
      // res.send('added unsuccessfully');
      res.json({ Error: 'Data cannot be added please check your input ' });
    }
  } catch (error) {
    res.send('added unsuccessfully');
    console.log(error);
  }
});
authorRouter.put('/update', async (req, res) => {
  let result;
  try {
    const {
      // eslint-disable-next-line camelcase
      Author_NAME, ShortInfo, Author_ID,
    } = req.body;
    result = await controller.editAuthor(Author_NAME, ShortInfo, Author_ID);
    if (result > 0) {
      res.send({ result });
    } else {
      res.send({ result });
    }
  } catch (error) {
    res.send(error);
  }
});
authorRouter.delete('/delete', async (req, res) => {
  let result;
  try {
    console.log(req.body);
    const { AuthorID } = req.body;
    result = await controller.deleteAuthor(AuthorID);
    if (result > 0) {
      res.send({ result });
    } else {
      res.send({ result });
    }
  } catch (error) {
    res.send(error);
  }
});

module.exports = authorRouter;
