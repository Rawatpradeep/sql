const mysql = require('promise-mysql');
const { config } = require('../configuration/config');

async function getAuthors() {
  let authors;
  const connection = await mysql.createConnection(config);
  try {
    const sql = 'SELECT * FROM author';
    authors = await connection.query(sql);
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
  return authors;
}

async function getAboutAuthor(id) {
  let author;
  const connection = await mysql.createConnection(config);
  try {
    const sql = `SELECT * FROM author WHERE Author_ID ='${id}';`;
    author = await connection.query(sql);
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
  return author;
}
// eslint-disable-next-line camelcase
async function addAuthor(Author_NAME, ShortInfo) {
  let result;
  let books;
  const connection = await mysql.createConnection(config);
  try {
    const sql = ' select * from author';
    books = await connection.query(sql);
    const ID = `${101 + books.length}`;
    const query = 'INSERT INTO author (Author_ID, Author_NAME, ShortInfo) VALUES (?,?,?)';
    // eslint-disable-next-line camelcase
    const values = [ID, Author_NAME, ShortInfo];
    try {
      result = await connection.query(query, values);
    } catch (error) {
      return error;
    }
  } catch (error) {
    console.log(error);
    return error;
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
// eslint-disable-next-line camelcase
async function editAuthor(Author_NAME, ShortInfo, Author_ID) {
  let result;
  const connection = await mysql.createConnection(config);
  // console.log(connection);
  try {
    const query = 'UPDATE author SET Author_NAME = ? , ShortInfo = ? where Author_ID = ?';
    // eslint-disable-next-line camelcase
    const values = [Author_NAME, ShortInfo, Author_ID];
    try {
      result = await connection.query(query, values);
    } catch (error) {
      return error;
    }
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
async function deleteAuthor(AuthorID) {
  let result;
  const connection = await mysql.createConnection(config);
  try {
    const query = 'DELETE FROM author where Author_ID = ?';
    result = await connection.query(query, [AuthorID]);
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
module.exports = {
  getAuthors,
  getAboutAuthor,
  addAuthor,
  editAuthor,
  deleteAuthor,
};
