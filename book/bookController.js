const mysql = require('promise-mysql');
const request = require('request');
const { parseString } = require('xml2js');
const { config } = require('../configuration/config');

// eslint-disable-next-line consistent-return
async function getAboutBook(id) {
  let book = [];
  const connection = await mysql.createConnection(config);
  try {
    const sql = `select * from book INNER JOIN author ON book.Author_ID = author.Author_ID WHERE Book_ID ='${id}';`;
    book = await connection.query(sql);
    if (book.length === 0) {
      return 'No Data Found';
    }
    return book;
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
}

function getReview(ISBN) {
  return new Promise((resolve, reject) => {
    const url = `https://www.goodreads.com/book/isbn/${ISBN}?key=YheuM1QbGHJD7l6iY1Mqgg`;
    request(url, (error, response, body) => {
      if (error) reject();
      const xml = body;
      // eslint-disable-next-line no-unused-expressions
      parseString(xml, async (err, result) => {
        const review = await result.GoodreadsResponse.book[0].reviews_widget;
        resolve(review);
      });
    });
  });
}


async function getBooks() {
  let books;
  const connection = await mysql.createConnection(config);
  try {
    const sql = ' select * from book INNER JOIN author ON book.Author_ID = author.Author_ID order by book.Book_ID';
    books = await connection.query(sql);
    if (books.length === 0) {
      return new Error('NO Data Found');
    }
  } catch (error) {
    console.log(error);
    return error;
  } finally {
    connection.end();
  }
  return books;
}
// eslint-disable-next-line camelcase
async function addBook(ISBN, Book_NAME, ABOUT, Author_ID, GENRE) {
  let result;
  let books;
  const connection = await mysql.createConnection(config);
  // console.log(connection);
  try {
    const sql = ' select * from book';
    books = await connection.query(sql);
    const ID = `${1 + books.length}`;
    const query = 'INSERT INTO book (Book_ID,ISBN, Book_NAME, ABOUT, Author_ID, GENRE) VALUES (?,?,?,?,?,?)';
    // eslint-disable-next-line camelcase
    const values = [ID, ISBN, Book_NAME, ABOUT, Author_ID, GENRE];
    try {
      result = await connection.query(query, values);
    } catch (error) {
      console.log(error);
      return 'Insertion Failed Please Give Proper data ';
    }
  } catch (error) {
    return error;
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
// eslint-disable-next-line camelcase
async function editBook(ISBN, Book_NAME, ABOUT, Author_ID, GENRE, Book_Id) {
  let result;
  const connection = await mysql.createConnection(config);
  // console.log(connection);
  try {
    const query = 'UPDATE book SET ISBN = ?, Book_NAME = ? , ABOUT = ?,Author_ID = ?, GENRE = ? where Book_ID= ?';
    // eslint-disable-next-line camelcase
    const values = [ISBN, Book_NAME, ABOUT, Author_ID, GENRE, Book_Id];
    try {
      result = await connection.query(query, values);
    } catch (error) {
      // console.log(error);
      return 'Updation Failed Please Give Proper data';
    }
  } catch (error) {
    // console.log(error);
    return error;
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
async function deleteBook(BookId) {
  let result = false;
  const connection = await mysql.createConnection(config);
  try {
    const query = 'DELETE FROM book where Book_ID = ?';
    result = await connection.query(query, [BookId]);
  } catch (error) {
    // console.log(error);
    return 'Deletion Failed Please Give Proper data';
  } finally {
    connection.end();
  }
  return result.affectedRows;
}
module.exports = {
  getBooks,
  getAboutBook,
  getReview,
  addBook,
  editBook,
  deleteBook,
};
