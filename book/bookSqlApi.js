const express = require('express');
const controller = require('./bookController');

const bookRouter = express.Router();

bookRouter.get('/api/getBooks', async (_req, res) => {
  try {
    const book = await controller.getBooks();
    if (book.length === 0) {
      return res.status(404).json({ error: 'NO Data Found' });
    }
    return res.send(book);
  } catch (error) {
    return res.status(404).render('error', { error });
  }
});

bookRouter.get('/api/getBooks/:ID', async (req, res) => {
  try {
    const books = await controller.getAboutBook(req.params.ID);
    if (books.length === 0) {
      return res.status(404).send('wrong BookID');
    }
    const review = await controller.getReview(books[0].ISBN);
    return res.json({ book: books[0], review });
  } catch (errors) {
    return res.status(404).render('error', { error: errors });
  }
});
bookRouter.post('/add', async (req, res) => {
  try {
    const {
      // eslint-disable-next-line camelcase
      ISBN, Book_NAME, ABOUT, Author_ID, GENRE,
    } = req.body;
    // eslint-disable-next-line camelcase
    if (Author_ID === 'Select Author') {
      res.send('please select a authorId');
    }
    const result = await controller.addBook(ISBN, Book_NAME, ABOUT, Author_ID, GENRE);
    if (result > 0) {
      res.json({ success: 'Data added successfully' });
      // res.send('added successfully');
    } else {
      res.json({ Error: 'Data cannot be added please check your input ' });
      // res.send('added unsuccessfully');
    }
  } catch (error) {
    console.log('error');
    res.send('added unsuccessfully');
  }
});
bookRouter.put('/update', async (req, res) => {
  let result = 0;
  try {
    const {
      // eslint-disable-next-line camelcase
      ISBN, Book_NAME, ABOUT, Author_ID, GENRE, Book_ID,
    } = req.body;
    result = await controller.editBook(ISBN, Book_NAME, ABOUT, Author_ID, GENRE, Book_ID);
    if (result > 0) {
      res.send({ result });
    } else {
      res.send({ result });
    }
  } catch (error) {
    res.send(error);
  }
});
bookRouter.delete('/delete', async (req, res) => {
  let result;
  try {
    const { BookId } = req.body;
    result = await controller.deleteBook(BookId);
    if (result > 0) {
      res.send({ result });
    } else {
      res.send(' unsuccessful');
    }
  } catch (error) {
    res.send(error);
  }
});
module.exports = bookRouter;
