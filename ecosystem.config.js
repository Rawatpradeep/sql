module.exports = {
  apps: [{
    name: 'sql',
    script: './server.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      type: 'production',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }],

  deploy: {
    production: {
      key: 'project.pem',
      user: 'ubuntu',
      host: '18.219.113.60',
      ref: 'origin/master',
      repo: 'git@gitlab.com:Rawatpradeep/sql.git',
      path: '/home/ubuntu/sql/',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production && npm start',
    },
  },
};
