const chai = require('chai').expect;
const getBookDetail = require('../../book/bookController');

const expect = chai;

describe('getDetails function test', () => {
  it('should return all the books data', async () => {
    const result = 'Harry Potter';
    const answer = await getBookDetail.getBooks();
    expect(answer[0].Book_NAME).to.equal(result);
  });
  it('should  books data of ID', async () => {
    const result = 'Harry Potter';
    const answer = await getBookDetail.getAboutBook(1);
    expect(answer[0].Book_NAME).to.equal(result);
  });
  it('should return data found if ID is wrong', async () => {
    const result = 'No Data Found';
    const answer = await getBookDetail.getAboutBook(10);
    expect(answer).to.equal(result);
  });
  it('should return true if books added successfully', async () => {
    const result = 1;
    const answer = await getBookDetail.addBook('1501180983', 'Book', 'this is a test case book', '106', 'Test');
    expect(answer).to.equal(result);
  });
  it('should return true if books updated successfully', async () => {
    const result = 1;
    const answer = await getBookDetail.editBook('1501180983', 'Bookss', 'this is a test case book', '105', 'Test', '7');
    expect(answer).to.equal(result);
  });
  it('should return true if books deleted successfully', async () => {
    const result = 1;
    const answer = await getBookDetail.deleteBook('7');
    expect(answer).to.equal(result);
  });
  it('should return error if books added successfully', async () => {
    const result = 'Insertion Failed Please Give Proper data ';
    const answer = await getBookDetail.addBook('1501180983', 'this is a test case book', '106', 'Test');
    expect(answer).to.equal(result);
  });
  it('should return 0 if books Update Fails', async () => {
    const result = 0;
    const answer = await getBookDetail.editBook('1501180983', 'this is a test case book', '106', 'Test', '8');
    expect(answer).to.equal(result);
  });
  it('should return 0 if books delete fails', async () => {
    const result = 0;
    const answer = await getBookDetail.deleteBook('10');
    expect(answer).to.equal(result);
  });
});
