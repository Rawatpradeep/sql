const request = require('supertest');
const { server } = require('../../server');


describe('BooksRouter', () => {
  after(() => {
    server.close();
  });
  it('returns all books', (done) => {
    request(server)
      .get('/books/api/getBooks')
      .expect(200)
      .expect(/Book_ID/)
      // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('returns details of book with particular ID', (done) => {
    request(server)
      .get('/books/api/getBooks/1')
      .expect(200)
      .expect(/Harry Potter/)
      // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  }).timeout(5000);
  it('add data to database', (done) => {
    request(server)
      .post('/books/add')
      .send({
        ISBN: '1501180983',
        Book_NAME: 'Pradeep',
        ABOUT: 'Fantastic book worth reading.',
        Author_ID: '105',
        GENRE: 'SAD',
      })
      .expect(/Data added successfully/)
      // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('returns updated successfully when edit successfully', (done) => {
    request(server)
      .put('/books/update')
      .send({
        ISBN: '12345123',
        Book_NAME: 'Pradeep',
        ABOUT: 'Fantastic book worth reading.',
        Author_ID: '105',
        GENRE: 'SAD',
        Book_ID: '7',
      })
      .expect(/1/)
    // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('returns updated successfully when edit successfully', (done) => {
    request(server)
      .delete('/books/delete')
      .send({
        BookId: '7',
      })
      .expect(/1/)
    // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
});
