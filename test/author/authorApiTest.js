const request = require('supertest');
const { server } = require('../../server');


describe('AuthorRouter', () => {
  after(() => {
    server.close();
  });
  it('returns all authors', (done) => {
    request(server)
      .get('/author/api/getAuthors')
      .expect(200)
      .expect(/Author_ID/)
      // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('add data to database', (done) => {
    request(server)
      .post('/author/add')
      .send({
        Author_NAME: 'Pradeep',
        ShortInfo: 'Fantastic book worth reading.',
      })
      .expect(/Data added successfully/)
      // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('returns updated successfully when edit successfully', (done) => {
    request(server)
      .put('/author/update')
      .send({
        Author_NAME: 'Pradeep',
        ShortInfo: 'Hi i am the author.',
        Author_ID: '107',
      })
      .expect(/1/)
    // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
  it('returns updated successfully when edit successfully', (done) => {
    request(server)
      .delete('/author/delete')
      .send({
        AuthorID: '107',
      })
      .expect(/1/)
    // eslint-disable-next-line consistent-return
      .end((err) => {
        if (err) return done(err);
        return done();
      });
  });
});
