const chai = require('chai').expect;
const getAuthorDetail = require('../../author/authorController');

const expect = chai;

describe('getDetails function test', () => {
  it('should return all the author data', async () => {
    const result = 'J. K. Rowling';
    const answer = await getAuthorDetail.getAuthors();
    expect(answer[0].Author_NAME).to.equal(result);
  });
  it('should return 1 if author added successfully', async () => {
    const result = 1;
    const answer = await getAuthorDetail.addAuthor('Pradeep', 'this is a test author');
    expect(answer).to.equal(result);
  });
  it('should return 1 if author updated successfully', async () => {
    const result = 1;
    const answer = await getAuthorDetail.editAuthor('Pradeep', 'hey iam edited', '107');
    expect(answer).to.equal(result);
  });
  it('should return 1 if author deleted successfully', async () => {
    const result = 1;
    const answer = await getAuthorDetail.deleteAuthor('107');
    expect(answer).to.equal(result);
  });
});
