const mysql = require('promise-mysql');
const logger = require('../configuration/logger');
const { config } = require('../configuration/config');
const bookData = require('./data');

async function insertTable() {
  const connection = await mysql.createConnection(config);
  try {
    const sql1 = 'INSERT INTO author (Author_NAME,ShortInfo,Bio) values ?';
    const authors = bookData.author.map(book => Object.values(book));
    await connection.query(sql1, [authors]);
    logger.info('data added to author table');
  } catch (error) {
    logger.info(error.message);
  }
  try {
    const sql2 = 'INSERT INTO book (ISBN,Book_NAME,ABOUT,Author_ID,GENRE,SUMMARY) values ?';
    const books = bookData.books.map(book => Object.values(book));
    await connection.query(sql2, [books]);
    logger.info('Data added to book table');
  } catch (error) {
    logger.info(error.message);
  } finally {
    connection.end();
  }
}

module.exports = {
  insertTable,
};

insertTable('DB');
