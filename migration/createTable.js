const mysql = require('promise-mysql');
const logger = require('../configuration/logger');
const { config } = require('../configuration/config');

async function createTable() {
  const connection = await mysql.createConnection(config);
  try {
    const sql1 = 'CREATE table IF NOT EXISTS author (Author_ID INT(7) PRIMARY KEY AUTO_INCREMENT, Author_NAME VARCHAR(15) NOT NULL, ShortInfo TINYTEXT, Bio TEXT );';
    await connection.query(sql1);
    const sql2 = 'ALTER TABLE author AUTO_INCREMENT=101;';
    await connection.query(sql2);
    logger.info('Author Table created');
  } catch (error) {
    logger.info(error.message);
  }
  try {
    const sql1 = 'CREATE table  IF NOT EXISTS book (Book_ID INT(7) PRIMARY KEY AUTO_INCREMENT,ISBN VARCHAR(10) UNIQUE, Book_NAME VarchAR(20) NOT NULL, ABOUT TINYTEXT, Author_ID Int(7) NOT NULL , GENRE VARCHAR(15), SUMMARY TEXT , FOREIGN KEY(Author_ID) REFERENCES author(Author_ID));';
    await connection.query(sql1);
    const sql2 = 'ALTER TABLE author AUTO_INCREMENT=101;';
    await connection.query(sql2);
    logger.info('Books Table created');
  } catch (error) {
    logger.info(error.message);
  } finally {
    connection.end();
  }
}

module.exports = {
  createTable,
};

createTable('DB');
