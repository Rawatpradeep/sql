const mysql = require('promise-mysql');
const logger = require('../configuration/logger');

let host = 'localhost';
let user = 'root';

if (process.env.type === 'production') {
  host = 'databaseforproject.cbmbmsund7sw.us-east-2.rds.amazonaws.com';
  user = 'Pradeep';
}

async function createDatabase(dbName) {
  const connection = await mysql.createConnection({
    host,
    user,
    password: 'Pradeep08',
  });
  try {
    const sql = `Create Database  IF NOT EXISTS ${dbName}`;
    await connection.query(sql);
    logger.info('Database created');
  } catch (error) {
    console.log(error.message);
  } finally {
    connection.end();
  }
}

module.exports = {
  createDatabase,
};
createDatabase('test');
