const loggers = require('./configuration/logger');

function logger(req, res, next) {
  loggers.info(`URL: ${req.url} Method: ${req.method}`);
  next();
}

module.exports = {
  logger,
};
